from django.db import models

# Create your models here.

class Year(models.Model):
    year = models.CharField(max_length=4, unique=True, primary_key=True)

    def __str__(self):
        return self.year

    class Meta:
        ordering = ['year']

class Friend(models.Model):
    full_name = models.CharField(max_length=100)
    student_id = models.CharField(max_length= 10, primary_key=True, unique=True)
    hobby = models.CharField(max_length=255)
    favorite_food = models.CharField(max_length=255)
    year = models.ForeignKey('Year', on_delete=models.CASCADE)