from django.shortcuts import render
from .models import Friend, Year
from .forms import Friend_Form

# Create your views here.

def index(request):
    return render(request, 'index.html')

def projects(request):

    return render(request, 'projects.html')

def story5(request):
    friend_list = Friend.objects.all()
    if request.method == 'POST':
        form = Friend_Form(request.POST)
        if form.is_valid():
            print(form.data)
            year = Year(form.data['year'])
            friend = Friend(full_name= form.data['name'], student_id = form.data['npm'], hobby = form.data['hobby'], favorite_food = form.data['favorite'], year = year)
            friend.save()
            blank = Friend_Form()
            form = {'form' : blank, 'friend_list' : friend_list}
            return render(request, 'story5.html', form)
    else:
        form = Friend_Form()  
        response = {'form' : form, 'friend_list' : friend_list}
        return render(request, 'story5.html', response)