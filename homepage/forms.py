from django import forms
from .models import Year

class Friend_Form(forms.Form):
    nameInput = {'id': 'forms', 'name' : 'fullname'}
    npmInput = {'id' : 'forms', 'name' : 'npm'}
    yearInput = {'id' : 'forms', 'name' : 'year'}
    year_choices = [('2016', '2016'), ('2017', '2017')]
    hobbyInput = {'id' : 'forms', 'name' : 'Hobby' }
    favInput = {'id' : 'forms', 'name' : 'Favorites'}

    name = forms.CharField(label='Name', required=True, max_length=100, widget=forms.TextInput(nameInput))

    npm = forms.CharField(label='NPM', required=True, max_length=10, widget=forms.TextInput(npmInput))

    hobby = forms.CharField(label='Hobby', required=True, max_length=255, widget=forms.TextInput(hobbyInput))

    favorite = forms.CharField(label='Favorites', required=True, max_length=255, widget=forms.TextInput(favInput))

    year = forms.ModelChoiceField(queryset = Year.objects.all() ,label='Year', required=True, widget=forms.Select(yearInput))
